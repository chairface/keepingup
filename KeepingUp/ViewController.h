//
//  ViewController.h
//  CoreImage_TEST
//
//  Created by Charlie Reiman on 7/10/13.
//  Copyright (c) 2013 Charlie Reiman. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (nonatomic, strong) IBOutlet UIImageView *topImage;
@property (nonatomic, strong) IBOutlet UIImageView *bottomImage;
@property (nonatomic, strong) IBOutlet UISlider *blurSlider;
@property (nonatomic, strong) IBOutlet UILabel *statsLabel;

- (IBAction) doValueChanged:(id)sender;

@end

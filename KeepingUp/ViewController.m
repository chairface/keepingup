//
//  ViewController.m
//  CoreImage_TEST
//
//  Created by Charlie Reiman on 7/10/13.
//  Copyright (c) 2013 Charlie Reiman. All rights reserved.
//

#import "ViewController.h"
#import <QuartzCore/QuartzCore.h>
#import <libkern/OSAtomic.h>

@interface ViewController ()
{
    EAGLContext *_eaglContext;
    CIContext *_ciContext;
    CIImage *_srcImage;
    CIFilter *_filter;
}
@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.topImage.image = [UIImage imageNamed:@"Bike.jpg"];
    _eaglContext = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
    _ciContext = [CIContext contextWithEAGLContext:_eaglContext options:@{kCIContextWorkingColorSpace: [NSNull null]}];
    _srcImage = [CIImage imageWithCGImage:self.topImage.image.CGImage];
    _filter = [CIFilter filterWithName:@"CIGaussianBlur"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/**
 * Perform some work (here I'm blurring an image) that may be too
 * expensive to run on every valueChanged event. This technique is
 * pretty simple. Only run the process if it is the only item of its
 * type on the queue. This insures old updates get ignored and won't
 * bog down the system.
 *
 * In this demo, I'm using the main queue as I want to update the UI
 * directly but you can do image filtering on a private thread if you
 * like. If you do, the OSAtomic updates are critical. As the demo
 * below is exclusive to the main thread, they aren't necessary.
 */
- (IBAction)doValueChanged:(id)sender
{
    static int32_t gOnQueue;  // This is the only one we need. The others are stats.
    static int32_t gDispatches;
    static int32_t gExecutions;
    static int32_t gRunningAvg;  // In microseconds.

    OSAtomicIncrement32(&gDispatches);
    OSAtomicIncrement32(&gOnQueue);

    dispatch_async(dispatch_get_main_queue(), ^{
        // Only run if we're the freshest of the fresh.
        if (OSAtomicDecrement32(&gOnQueue) == 0) {
            OSAtomicIncrement32(&gExecutions);
            
            CFTimeInterval delta = CACurrentMediaTime();
            [_filter setValue:_srcImage forKey:kCIInputImageKey];
            [_filter setValue:[NSNumber numberWithFloat:self.blurSlider.value] forKey:@"inputRadius"];
            CIImage *result = [_filter valueForKey:kCIOutputImageKey];
            CGImageRef cgImage = [_ciContext createCGImage:result fromRect:[result extent]];
            delta = CACurrentMediaTime() - delta;

            // Maintain the running average.
            int32_t oldAvg;
            int32_t newAvg;
            do {
                oldAvg = gRunningAvg;
                newAvg = (3*oldAvg + (int)(delta*1.e6))/4;
            }
            while(!OSAtomicCompareAndSwap32(oldAvg, newAvg, &gRunningAvg));

            self.bottomImage.image = [UIImage imageWithCGImage:cgImage];
            CGImageRelease(cgImage);
        }
    });

    // Let's use the same technique to update the UI's stats. This is
    // a little simpler as I'm not maintaining any stats about stats.
    
    static int32_t gStatsOnQueue;
    OSAtomicIncrement32(&gStatsOnQueue);
    dispatch_async(dispatch_get_main_queue(), ^{
        if (OSAtomicDecrement32(&gStatsOnQueue) == 0) {
            float percent = 100.0f * gExecutions;
            if (gDispatches != 0) {
                percent /= (float)gDispatches;
            }
            else {
                percent = 0;
            }
            self.statsLabel.text = [NSString stringWithFormat:@"%d/%d %.3g%%, %.1f ms", gExecutions, gDispatches, percent, gRunningAvg/1000.0];
        }
    });
}

@end
